FROM openjdk:alpine
ADD target/*.jar app.jar
RUN sh -c 'touch /app.jar'
ENV SPRING_PROFILES_ACTIVE 'cloud,security'
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","com.promeets.service.resource.ResourceApplication"]

