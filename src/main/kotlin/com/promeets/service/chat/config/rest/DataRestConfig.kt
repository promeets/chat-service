package com.promeets.service.chat.config.rest

import com.promeets.service.chat.model.Chat
import com.promeets.service.chat.model.Participant
import com.promeets.service.chat.model.User
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter


@Configuration
open class DataRestConfig : RepositoryRestConfigurerAdapter() {
    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
        config.exposeIdsFor(
                Participant::class.java,
                User::class.java,
                Chat::class.java
        )
    }
}