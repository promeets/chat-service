package com.promeets.service.chat.config.security

import com.promeets.service.chat.config.spel.SecurityEvaluationContextExtension
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.repository.query.spi.EvaluationContextExtension
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import javax.servlet.http.HttpServletResponse


@Configuration
@EnableConfigurationProperties(OpenSecurityProperties::class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
open class OpenSecurityConfig(
        val userDetailService: OpenUserDetailService,
        val openSecurityProperties: OpenSecurityProperties
) : WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailService)
    }

    override fun configure(http: HttpSecurity) {
        val welcomeText = "Use this default credentials: ${openSecurityProperties.email} : ${openSecurityProperties.password}";
        http
                .httpBasic()
                .authenticationEntryPoint { _, response, _ ->
                    response.addHeader("WWW-Authenticate", "Basic realm=\"$welcomeText\"")
                    response.status = HttpServletResponse.SC_UNAUTHORIZED
                    response.outputStream.print(welcomeText)
                }.and()
                .rememberMe().and()
                .csrf().disable()
                .authorizeRequests().anyRequest().authenticated()
    }
}