package com.promeets.service.chat.config.security

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "security.open")
open class OpenSecurityProperties {
    var password: String = "password"
    var email: String = "user"
}
