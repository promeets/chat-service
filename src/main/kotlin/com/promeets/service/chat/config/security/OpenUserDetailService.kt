package com.promeets.service.chat.config.security

import com.promeets.service.chat.model.User
import com.promeets.service.chat.repository.UserRepository
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
open class OpenUserDetailService(
        val userRepository: UserRepository,
        val openSecurityProperties: OpenSecurityProperties
) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByEmail(username) ?: User()
        return org.springframework.security.core.userdetails.User(
                user.email,
                openSecurityProperties.password,
                AuthorityUtils.createAuthorityList("ROLE_USER")
        )
    }
}