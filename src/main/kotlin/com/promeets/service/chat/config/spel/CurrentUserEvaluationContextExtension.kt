package com.promeets.service.chat.config.spel

import com.promeets.service.chat.service.UserService
import org.springframework.data.repository.query.spi.EvaluationContextExtensionSupport
import org.springframework.stereotype.Component

@Component
open class CurrentUserEvaluationContextExtension(
        val userService: UserService
) : EvaluationContextExtensionSupport() {

    override fun getExtensionId() = "currentUser"

    override fun getRootObject() = userService.currentUser()
}