package com.promeets.service.chat.config.spel

import org.springframework.data.repository.query.spi.EvaluationContextExtensionSupport
import org.springframework.security.access.expression.SecurityExpressionRoot
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
open class SecurityEvaluationContextExtension : EvaluationContextExtensionSupport() {
    override fun getExtensionId(): String {
        return "security"
    }

    override fun getRootObject(): SecurityExpressionRoot {
        val authentication = SecurityContextHolder.getContext().authentication
        return object : SecurityExpressionRoot(authentication) {}
    }
}