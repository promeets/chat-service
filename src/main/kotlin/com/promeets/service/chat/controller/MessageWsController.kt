package com.promeets.service.chat.controller

import com.promeets.service.chat.model.Message
import com.promeets.service.chat.model.WsChatMessage
import com.promeets.service.chat.repository.ChatRepository
import com.promeets.service.chat.repository.MessageRepository
import com.promeets.service.chat.repository.UserRepository
import com.promeets.service.chat.service.UserService
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.socket.WebSocketSession
import java.security.Principal


@Controller
class MessageWsController(
        val userService: UserService,
        val chatRepository: ChatRepository,
        val simpMessageTemplate: SimpMessagingTemplate,
        val messageRepository: MessageRepository,
        val userRepository: UserRepository
) {


    @MessageMapping("/chat/message")
    fun message(wsMessage: WsChatMessage, principal: Principal) {
        val chat = chatRepository.findOne(wsMessage.getChatId())
        val user = userService.currentUser(principal)
        val message = Message(
                user = user,
                chat = chat,
                text = wsMessage.getText(),
                time = wsMessage.getTime(),
                action = wsMessage.getAction(),
                icon = wsMessage.getIcon()
        )
        wsMessage.setId(messageRepository.save(message).id)
        wsMessage.setUserId(user.id)
        userRepository.findByChatId(chat.id)
                .forEach {
                    simpMessageTemplate.convertAndSend("/topic/chat/message/${it.id}", wsMessage)
                }
    }
}