package com.promeets.service.chat.model

import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "chats")
data class Chat(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
        @Column var name: String = "",
        @Column var lastUpdate: Timestamp = Timestamp(Date().time),
        @OneToMany(mappedBy = "chat") val participants: MutableList<Participant> = mutableListOf(),
        @OneToMany(mappedBy = "chat") val messages: MutableList<Message> = mutableListOf()
)
