package com.promeets.service.chat.model

import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "messages")
data class Message(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
        @Column var text: String = "",
        @Column val time: Timestamp = Timestamp(Date().time),
        @Column val action: String = "",
        @Column val icon: String = "",
        @ManyToOne val user: User? = null,
        @ManyToOne val chat: Chat? = null
)