package com.promeets.service.chat.model

import javax.persistence.*

@Entity
@Table(name = "participants")
data class Participant(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
        @Column var chatName: String = "",
        @ManyToOne @JoinColumn val user: User? = null,
        @ManyToOne @JoinColumn val chat: Chat? = null
) {
    val userId get() = user?.id
    val chatId get() = chat?.id

}