package com.promeets.service.chat.model

import javax.persistence.*

@Entity
@Table(name = "users")
data class User (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
        @Column(unique = true) var email: String? = null,
        @Column var firstName: String? = null,
        @Column var lastName: String? = null
) {
    val fullName get() = "$firstName $lastName"
}