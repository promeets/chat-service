package com.promeets.service.chat.model

import com.promeets.service.chat.model.projection.DefaultMessagesProjection
import org.json.JSONObject
import java.sql.Timestamp
import java.util.*

class WsChatMessage(
        private var id: Long? = null,
        private var text: String = "",
        private var time: Timestamp = Timestamp(Date().time),
        private var action: String = "",
        private var icon: String = "",
        private var chatId: Long? = null,
        private var userId: Long? = null
) : DefaultMessagesProjection {
    override fun getId() = id!!
    fun setId(id: Long) {
        this.id = id
    }

    override fun getText() = text

    override fun getTime() = time

    override fun getAction() = action

    override fun getIcon() = icon

    override fun getChatId() = chatId!!

    override fun getUserId() = userId!!
    fun setUserId(userId: Long) {
        this.userId = userId
    }
}