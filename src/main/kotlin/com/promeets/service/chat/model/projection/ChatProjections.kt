package com.promeets.service.chat.model.projection

import com.promeets.service.chat.model.Chat
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection
import java.sql.Timestamp

@Projection(name = "default", types = arrayOf(Chat::class))
interface ChatDefaultProjection {

    fun getId(): Long

    @Value("#{@chatService.getNameForCurrentUser(target)}")
    fun getName(): String

    fun getLastUpdate(): Timestamp
}