package com.promeets.service.chat.model.projection

import com.promeets.service.chat.model.Message
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection
import java.sql.Timestamp

@Projection(name = "default", types = arrayOf(Message::class))
interface DefaultMessagesProjection {
    fun getId(): Long

    fun getText(): String

    fun getTime():Timestamp

    fun getAction(): String

    fun getIcon(): String

    @Value("#{target.user.id}")
    fun getUserId(): Long

    @Value("#{target.chat.id}")
    fun getChatId(): Long

}

