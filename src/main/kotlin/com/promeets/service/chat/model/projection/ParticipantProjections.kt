package com.promeets.service.chat.model.projection

import com.promeets.service.chat.model.Participant
import com.promeets.service.chat.model.User
import org.springframework.data.rest.core.config.Projection

@Projection(name = "default", types = arrayOf(Participant::class))
interface ParticipantsDefaultProjection {
    fun getId(): Long
    fun getUserId(): Long
    fun getChatId(): Long
}

@Projection(name = "inlineUser", types = arrayOf(Participant::class))
interface ParticipantsInlineUserProjection : ParticipantsDefaultProjection {
    fun getUser(): User
}