package com.promeets.service.chat.repository

import com.promeets.service.chat.model.Chat
import com.promeets.service.chat.model.projection.ChatDefaultProjection
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(excerptProjection = ChatDefaultProjection::class)
interface ChatRepository : PagingAndSortingRepository<Chat, Long> {
    @RestResource(path = "me", rel = "me")
    @Query("select chat from Chat chat,Participant p where p.user.id = ?#{currentUser.id} and chat.id = p.chat.id")
    fun findByCurrentUser(pageable: Pageable): Page<Chat>

    @RestResource(path = "meByName", rel = "meByName")
    @Query("select chat from Chat chat,Participant p where p.user.id = ?#{currentUser.id} and chat.id = p.chat.id and lower(p.chatName) like lower(concat('%', :name,'%'))")
    fun findByCurrentUserAndName(@Param("name") name: String, pageable: Pageable): Page<Chat>
}