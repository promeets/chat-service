package com.promeets.service.chat.repository

import com.promeets.service.chat.model.Message
import com.promeets.service.chat.model.projection.DefaultMessagesProjection
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(excerptProjection = DefaultMessagesProjection::class)
interface MessageRepository : PagingAndSortingRepository<Message, Long> {

    @RestResource(rel = "byChat", path = "byChat")
    @Query("select m from Message m where m.chat.id = :chatId")
    fun byChat(@Param("chatId") chatId: Long, pageable: Pageable): Page<Message>

}