package com.promeets.service.chat.repository

import com.promeets.service.chat.model.Participant
import com.promeets.service.chat.model.projection.ParticipantsDefaultProjection
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(excerptProjection = ParticipantsDefaultProjection::class)
interface ParticipantRepository : PagingAndSortingRepository<Participant, Long> {

    @RestResource(path = "me", rel = "me")
    @Query("select p from Participant p where p.user.id = ?#{currentUser.id}")
    fun findByCurrentUser(pageable: Pageable): Page<Participant>

    @RestResource(path = "meByChat", rel = "meByChat")
    @Query("select p from Participant p where p.user.id = ?#{currentUser.id} and p.chat.id = :chatId")
    fun findByCurrentUserAndChat(@Param("chatId") chatId: Long): Participant?

}

