package com.promeets.service.chat.repository

import com.promeets.service.chat.model.User
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RestResource

interface UserRepository : PagingAndSortingRepository<User, Long> {
    fun findByEmail(@Param("email") email: String): User?

    @RestResource(path = "byName", rel = "byName")
    @Query("""
            select u from User u
                where (lower(u.firstName) like lower(concat('%',:p1,'%')) and lower(u.lastName) like lower(concat('%',:p2,'%')))
                        or
                      (lower(u.firstName) like lower(concat('%',:p2,'%')) and lower(u.lastName) like lower(concat('%',:p1,'%')))
    """)
    fun findByName(
            @Param("p1") p1: String,
            @Param("p2") p2: String,
            pageable: Pageable
    ): Page<User>

    @RestResource(path = "byChat", rel = "byChat")
    @Query("select p.user from Participant p where p.chat.id = :chatId")
    fun findByChatId(@Param("chatId") chatId: Long): Iterable<User>

    @RestResource(path = "me", rel = "me")
    @Query("select u from User u where u.email = :#{authentication?.name}")
    fun findCurrentUser(): User?
}
