package com.promeets.service.chat.service

import com.promeets.service.chat.model.Chat

interface ChatService {
    fun getNameForCurrentUser(chat: Chat): String
}