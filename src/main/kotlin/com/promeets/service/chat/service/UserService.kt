package com.promeets.service.chat.service

import com.promeets.service.chat.model.User
import java.security.Principal

interface UserService {
    fun currentUser(): User
    fun currentUser(principal: Principal): User
}