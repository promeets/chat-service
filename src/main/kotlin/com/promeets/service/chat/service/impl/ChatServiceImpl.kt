package com.promeets.service.chat.service.impl

import com.promeets.service.chat.model.Chat
import com.promeets.service.chat.repository.ParticipantRepository
import com.promeets.service.chat.service.ChatService
import com.promeets.service.chat.service.UserService
import org.springframework.stereotype.Service

@Service("chatService")
class ChatServiceImpl(
        val userService: UserService,
        val participantRepository: ParticipantRepository
) : ChatService {
    override fun getNameForCurrentUser(chat: Chat): String {
        return participantRepository.findByCurrentUserAndChat(chat.id)?.chatName ?: "undefined"
    }
}