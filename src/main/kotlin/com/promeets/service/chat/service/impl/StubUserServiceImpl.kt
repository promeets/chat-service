package com.promeets.service.chat.service.impl

import com.promeets.service.chat.model.User
import com.promeets.service.chat.service.UserService
import com.promeets.service.chat.repository.UserRepository
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.logging.Logger

@Service
class StubUserServiceImpl(
        val userRepository: UserRepository
) : UserService {


    val log: Logger = Logger.getLogger(StubUserServiceImpl::class.toString())
    val STUB_USER_EMAIL: String = "sara@mail.com"

    override fun currentUser(principal: Principal): User {
        return findUser(principal.name)
    }

    override fun currentUser(): User {
        return findUser(
                SecurityContextHolder
                        .getContext()
                        .authentication
                        .name
        )

    }

    private fun findUser(username: String): User {
        return userRepository.findByEmail(username)
                ?:
                {
                    log.warning("Unable to get current user from principal. Using stub user: $STUB_USER_EMAIL")
                    userRepository.findByEmail(STUB_USER_EMAIL)!!
                }()
    }

}