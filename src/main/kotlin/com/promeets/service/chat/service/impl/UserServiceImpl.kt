package com.promeets.service.chat.service.impl

import com.promeets.service.chat.model.User
import com.promeets.service.chat.service.UserService
import com.promeets.service.chat.repository.UserRepository
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.security.Principal

@Service
@Primary
@Profile("security")
class UserServiceImpl(
        val userRepository: UserRepository
) : UserService {
    override fun currentUser(principal: Principal): User {
        return findUser(principal.name)
    }

    override fun currentUser(): User {
        return findUser(
                SecurityContextHolder
                        .getContext()
                        .authentication
                        .name
        )
    }

    //ToDo: change NPE to something more appropriate
    private fun findUser(username: String): User {
        return userRepository.findByEmail(username)!!
    }
}