CREATE TABLE "chats" (
  "id"   bigserial NOT NULL,
  "name" TEXT,
  "last_update" TIMESTAMP,
  CONSTRAINT chats_pk PRIMARY KEY ("id")
) WITH (
OIDS = FALSE
);


CREATE TABLE participants (
  "id"      bigserial NOT NULL,
  "chat_id" BIGINT NOT NULL,
  "user_id" BIGINT NOT NULL,
  "chat_name" TEXT,
  CONSTRAINT participants_pk PRIMARY KEY ("id")
) WITH (
OIDS = FALSE
);


CREATE TABLE "users" (
  "id"         bigserial NOT NULL,
  "first_name" TEXT   NOT NULL,
  "last_name"  TEXT   NOT NULL,
  "email"      TEXT   NOT NULL UNIQUE,
  CONSTRAINT users_pk PRIMARY KEY ("id")
) WITH (
OIDS = FALSE
);

CREATE TABLE messages (
  "id"         bigserial NOT NULL,
  "text" TEXT,
  "chat_id" BIGINT NOT NULL,
  "user_id" BIGINT NOT NULL,
  "time" TIMESTAMP NOT NULL,
  "action" TEXT,
  "icon" TEXT,
  CONSTRAINT message_pk PRIMARY KEY ("id")
) WITH (
OIDS = FALSE
);


ALTER TABLE participants
  ADD CONSTRAINT "participants_fk0" FOREIGN KEY ("chat_id") REFERENCES chats ("id");
ALTER TABLE participants
  ADD CONSTRAINT "participants_fk1" FOREIGN KEY ("user_id") REFERENCES users ("id");

ALTER TABLE messages
  ADD CONSTRAINT "messages_fk0" FOREIGN KEY ("chat_id") REFERENCES chats ("id");
ALTER TABLE messages
  ADD CONSTRAINT "messages_fk1" FOREIGN KEY ("user_id") REFERENCES users ("id");
