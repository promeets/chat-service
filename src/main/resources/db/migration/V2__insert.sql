INSERT INTO users ("first_name", "last_name", email)
VALUES ('Sara', 'Connor', 'sara@mail.com');

INSERT INTO users ("first_name", "last_name", email)
VALUES ('John', 'Smith', 'john@mail.com');

INSERT INTO users ("first_name", "last_name", email)
VALUES ('Michael', 'Jennet', 'michiael@mail.com');

INSERT INTO chats (name, last_update) VALUES ('Xab', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Ybc', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Zde', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Afg', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Bhi', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Fjk', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Glm', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Rlm', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Uno', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Lqr', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Mqr', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Nsr', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Sst', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Vee', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Wdf', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Ksd', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Cfd', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Dvc', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Exx', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Tfg', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Hsd', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Izx', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Jkj', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Oas', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Pds', CURRENT_TIMESTAMP);
INSERT INTO chats (name, last_update) VALUES ('Qss', CURRENT_TIMESTAMP);


INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (1, 1, 'Xab');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (1, 2, 'Xab');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (1, 3, 'Xab');

INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (2, 1, 'John Smith');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (2, 2, 'Sara Connor');

INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (3, 1, 'Zde');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (4, 1, 'Afg');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (5, 1, 'Bhi');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (6, 1, 'Fjk');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (7, 1, 'Glm');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (8, 1, 'Rlm');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (9, 1, 'Uno');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (10, 1, 'Lqr');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (11, 1, 'Mqr');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (12, 1, 'Nsr');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (13, 1, 'Sst');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (14, 1, 'Vee');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (15, 1, 'Wdf');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (16, 1, 'Ksd');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (17, 1, 'Cfd');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (18, 1, 'Dvc');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (19, 1, 'Exx');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (20, 1, 'Tfg');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (21, 1, 'Hsd');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (22, 1, 'Izx');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (23, 1, 'Jkj');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (24, 1, 'Oas');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (25, 1, 'Pds');
INSERT INTO participants ("chat_id", "user_id", "chat_name") VALUES (26, 1, 'Qss');

INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());
INSERT INTO messages (text, chat_id, user_id, time) VALUES ('Text', 1, 1, now());